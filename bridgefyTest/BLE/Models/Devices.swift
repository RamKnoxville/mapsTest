//
//  Devices.swift
//  bridgefyTest
//
//  Created by Ram on 22/04/21.
//

import Foundation

class Devices{
    
    var _name:String!
    var _rssi:String!
    
    var name: String {
        return _name
    }
    
    var rssi: String {
        return _rssi
    }
    
    init(name: String, rssi: String) {
        self._name = name
        self._rssi = rssi
    }
    
}
