//
//  Borders.swift
//  bridgefyTest
//
//  Created by Ram on 22/04/21.
//

import Foundation

class Borders{
    
    var _name:String!
    
    var name: String {
        return _name
    }
    
    init(name:String) {
        self._name = name
    }
    
}
