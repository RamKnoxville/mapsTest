//
//  ViewController.swift
//  bridgefyTest
//
//  Created by Ram on 21/04/21.
//

import UIKit
import SCLAlertView

class ViewController: UIViewController {
    
    @IBOutlet weak var usrEmail:UITextField!
    @IBOutlet weak var usrPwd:UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    @IBAction func loginBtnPressed(_ sender:Any){
        
        if usrEmail.text == "" || usrPwd.text == "" {
            SCLAlertView().showWarning("Alerta", subTitle: "Debes llenar todos los campos para continuar")
        }else{
            LoginAPI().login(email: usrEmail.text!, password: usrPwd.text!) { (response, statusCode) in
                
                switch statusCode {
                case 200:
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.02) {
                        self.performSegue(withIdentifier: "goToMain", sender: nil)
                    }
                    
                default:
                    SCLAlertView().showError("Error", subTitle: "Usuario ó contraseña no válidos")
                }
            }
        }
        
    }


}

