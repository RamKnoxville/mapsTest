//
//  BLEVC.swift
//  bridgefyTest
//
//  Created by Ram on 22/04/21.
//

import UIKit
import SCLAlertView
import CoreBluetooth

class BLEVC: UIViewController, UITableViewDelegate, UITableViewDataSource, CBCentralManagerDelegate {
    
    @IBOutlet weak var scannedDevicesTable:UITableView!
    @IBOutlet weak var spinner:UIActivityIndicatorView!
    @IBOutlet weak var spinnerView:UIView!

    var devicesFound = [Devices]()
    var devicesDic = [String:Devices]()
    var centralManager: CBCentralManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.spinnerView.isHidden = true
        self.spinner.stopAnimating()
        
        self.scannedDevicesTable.delegate = self
        self.scannedDevicesTable.dataSource = self
        self.scannedDevicesTable.tableFooterView = UIView()
        
        centralManager = CBCentralManager(delegate: self, queue: nil)
        
    }
    
//    MARK: TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.devicesFound.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = scannedDevicesTable.dequeueReusableCell(withIdentifier: "DevicesCell") as! DevicesCell
        
        cell.configDevicesCell(device: self.devicesFound[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 102
    }
    
//    MARK: CoreBluetooth methods
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .unknown:
            print("unknown")
        case .resetting:
            print("resetting")
        case .unsupported:
            print("unsupported")
        case .unauthorized:
            print("unauthorized")
        case .poweredOff:
            print("poweredOff")
        case .poweredOn:
            print("poweredOn")
        @unknown default:
            print("default")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral,
                        advertisementData: [String: Any], rssi RSSI: NSNumber) {
      
        let name = peripheral.name ?? ""
        let id = peripheral.identifier
        let rssi = RSSI

        if name == "" {
            return
        }
        
        let bleObj = Devices(name: name, rssi: "ID:\(id) RSSI:\(rssi)")
        if self.devicesDic["\(id)"] == nil {
            self.devicesDic["\(id)"] = bleObj
            self.devicesFound.append(bleObj)
            self.scannedDevicesTable.reloadData()
        }
        
     
    }

    
    
//    MARK: IBActions
    
    @IBAction func scanBtnPressed(_ sender:Any){
        
        self.spinnerView.isHidden = false
        self.spinner.startAnimating()
        self.devicesDic.removeAll()
        self.devicesFound.removeAll()
        self.scannedDevicesTable.reloadData()
        
        centralManager.scanForPeripherals(withServices: nil, options: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 30.0) {
            self.spinnerView.isHidden = true
            self.spinner.stopAnimating()
            self.centralManager.stopScan()
            SCLAlertView().showSuccess("Escaneo listo", subTitle: "")
        }
        
    }

}
