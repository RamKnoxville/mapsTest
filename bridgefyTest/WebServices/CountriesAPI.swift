//
//  CountriesAPI.swift
//  bridgefyTest
//
//  Created by Ram on 22/04/21.
//

import Foundation
import SwiftyJSON
import Alamofire

class CountriesAPI {
    
    enum CountriesURL {
        case getCountries
        case countryDetail
        func getURLString() -> String {
            switch self {
            case .getCountries:
                return "https://restcountries-v1.p.rapidapi.com/all"
            case .countryDetail:
                return "https://restcountries-v1.p.rapidapi.com/alpha/"
            }
        }
    }
    
    
    func getAllCountries(completion: @escaping (_ response: JSON, _ statusCode: Int) -> ()){
        
        RestAPI().generalRequest(CountriesURL.getURLString(.getCountries)()) { response, statusCode in
            completion(response, statusCode)
        }

    }
    
    func getCountryDetail(countryCode:String, completion: @escaping (_ response: JSON, _ statusCode: Int) -> ()){
        
        let url = "\(CountriesURL.getURLString(.countryDetail)())\(countryCode)"
        
        RestAPI().generalRequest(url) { response, statusCode in
            completion(response, statusCode)
        }
    }
    
}
