//
//  CountriesVC.swift
//  bridgefyTest
//
//  Created by Ram on 21/04/21.
//

import UIKit
import SwiftyJSON
import SCLAlertView

class CountriesVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var countriesTable:UITableView!
    @IBOutlet weak var searchField:UISearchBar!
    @IBOutlet weak var groupBtn:UIButton!
    @IBOutlet weak var spinnerView:UIView!
    @IBOutlet weak var spinner:UIActivityIndicatorView!
    
    var countries = [Countries]()
    
    var filteredCountries = [Countries]()
    
    var countriesByGroup = [String : [Countries]]()
    
    var regions = [String]()
    
    var groups = [String]()
    
    var filterMode = false
    
    var groupedMode = false
    
    var countryCode = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        self.spinnerView.isHidden = true
        self.spinner.isHidden = true
        self.spinner.stopAnimating()
        
        self.countriesTable.delegate = self
        self.countriesTable.dataSource = self
        self.countriesTable.tableFooterView = UIView()
        
        self.searchField.delegate = self
        
        self.getCountries()
        
    }
    
//    MARK: Business Logic Methods
    
    func getCountries(){
        
        self.spinnerView.isHidden = false
        self.spinner.isHidden = false
        self.spinner.startAnimating()
        
        CountriesAPI().getAllCountries { (jsonResponse, statusCode) in
            
            switch statusCode {
            case 200:
                self.iterateCountries(jsonResponse)
            default :
                SCLAlertView().showError("Error", subTitle: "\(jsonResponse)")
            }
            
        }
    }
    
    func iterateCountries(_ contenido:JSON){

        let str = contenido.description
        let data = Data(str.utf8)

        do {
            // make sure this JSON is in the format we expect
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? Array<Any> {
                
                for country in json {
                    let dict = country as! Dictionary<String,Any>
                    let name = "\(dict["name"] ?? "")"
                    let alpha2 = "\(dict["alpha2Code"] ?? "")"
                    let alpha3 = "\(dict["alpha3Code"] ?? "")"
                    let region = "\(dict["region"] ?? "")"
                    let countryObject = Countries(name: name, alpha2: alpha2, alpha3: alpha3, region: region)
                    self.countries.append(countryObject)
                    
                }
                
                self.countriesTable.reloadData()
                
            }
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
        
        self.spinnerView.isHidden = true
        self.spinner.isHidden = true
        self.spinner.stopAnimating()

    }
    
//    MARK: TableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.groupedMode == false ? 1 : self.regions.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.groupedMode == false ? nil : regions[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.groupedMode {
            let region = self.regions[section]
            return self.countriesByGroup[region]!.count
        }else{
            return self.filterMode == false ? self.countries.count : self.filteredCountries.count
        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.countriesTable.dequeueReusableCell(withIdentifier: "CountriesCell") as! CountriesCell
        
        if self.groupedMode {
            
            let region = regions[indexPath.section]
            let regionArray = countriesByGroup[region]!
            let country = regionArray[indexPath.row]

            cell.configureCell(country: country)
            
        }else{
            
            let country = self.filterMode == false ? self.countries[indexPath.row] : self.filteredCountries[indexPath.row]
            
            cell.configureCell(country: country)
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.countryCode = self.filterMode == false ? self.countries[indexPath.row]._alpha2 : self.filteredCountries[indexPath.row]._alpha2
        
        self.performSegue(withIdentifier: "goToDetail", sender: self.countryCode)
        
    }
    
    
//    MARK: SearchField Methods
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let searchText  = self.searchField.text?.uppercased()
        
        if searchText == "" {
            filterMode = false
            self.countriesTable.reloadData()
            return
        }
        
        filterMode = true
        self.filteredCountries = self.countries.filter({$0.name.uppercased().contains(searchText!)})
        self.countriesTable.reloadData()
        
    }
    
    
    @IBAction func groupCountriesBtnPressed(_ sender:Any){
        
        self.groupedMode = self.groupedMode == false ? true : false
        self.groupBtn.setTitle(self.groupedMode == false ? "GROUP" : "UNGROUP", for: .normal)
        self.searchField.isHidden = self.groupedMode == false ? false : true
        
        if self.groupedMode {
            self.countriesByGroup = Dictionary(grouping: self.countries, by: {$0._region})
            for value in countries {
                if !regions.contains(value._region) {
                    regions.append(value._region)
                }
            }
        }
        
        self.countriesTable.reloadData()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToDetail" {
            let countryDetail = segue.destination as! CountryDetailVC
            countryDetail.countryCode = self.countryCode
        }
    }
    
}
