//
//  LoginAPI.swift
//  bridgefyTest
//
//  Created by Ram on 21/04/21.
//

import Foundation

class LoginAPI {
    
    func login(email:String, password:String, completion: @escaping (_ response: Bool, _ statusCode: Int) -> ()){

        if email == "challenge@bridgefy.me" && password == "P@$$w0rD!" {
            completion(true, 200)
        }else{
            completion(false, 0)
        }

    }
    
}
