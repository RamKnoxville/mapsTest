//
//  Countries.swift
//  bridgefyTest
//
//  Created by Ram on 21/04/21.
//

import Foundation

class Countries {
    
    var _name:String!
    var _alpha2:String!
    var _alpha3:String!
    var _region:String!
    
    var name: String {
        return _name
    }
    
    var alpha2: String {
        return _alpha2
    }
    
    var alpha3: String {
        return _alpha3
    }
    
    var region: String {
        return _region
    }
    
    init(
        name:String,
        alpha2:String,
        alpha3:String,
        region:String
        ) {
        self._name = name
        self._alpha2 = alpha2
        self._alpha3 = alpha3
        self._region = region
    }
    
}
