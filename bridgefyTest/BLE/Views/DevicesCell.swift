//
//  DevicesCell.swift
//  bridgefyTest
//
//  Created by Ram on 22/04/21.
//

import UIKit

class DevicesCell: UITableViewCell {

    @IBOutlet weak var deviceName:UILabel!
    @IBOutlet weak var deviceRssi:UILabel!
    
    func configDevicesCell(device:Devices){
        
        self.deviceName.text = device._name
        self.deviceRssi.text = device._rssi
        
    }

}
