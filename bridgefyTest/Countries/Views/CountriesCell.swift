//
//  CountriesCell.swift
//  bridgefyTest
//
//  Created by Ram on 21/04/21.
//

import UIKit

class CountriesCell: UITableViewCell {

    @IBOutlet weak var countryFlag:UILabel!
    @IBOutlet weak var countryName:UILabel!
    @IBOutlet weak var countryRegion:UILabel!
    
    func configureCell(country:Countries){
        
        self.countryFlag.text = "\(GeneralHelpers().flag(country._alpha2))"
        self.countryName.text = country._name
        self.countryRegion.text = "\(country._alpha2 ?? "") / \(country._alpha3 ?? "")"
        
    }
    
}
