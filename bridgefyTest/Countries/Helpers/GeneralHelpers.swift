//
//  GeneralHelpers.swift
//  bridgefyTest
//
//  Created by Ram on 22/04/21.
//

import Foundation

class GeneralHelpers {
    
    func flag(_ country:String) -> String {
        let base : UInt32 = 127397
        var s = ""
        for v in country.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
    
}
