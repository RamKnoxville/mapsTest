//
//  CountryDetailVC.swift
//  bridgefyTest
//
//  Created by Ram on 22/04/21.
//

import UIKit
import SCLAlertView
import SwiftyJSON

class CountryDetailVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var countryNameTitle:UILabel!
    @IBOutlet weak var mainFlagImage:UIImageView!
    @IBOutlet weak var countryMap:UIImageView!
    @IBOutlet weak var countryNativeName:UILabel!
    @IBOutlet weak var countryNameCapital:UILabel!
    @IBOutlet weak var countrySubRegion:UILabel!
    @IBOutlet weak var countryArea:UILabel!
    @IBOutlet weak var countryCoordinates:UILabel!
    @IBOutlet weak var countryPopulation:UILabel!
    @IBOutlet weak var countryLanguage:UILabel!
    @IBOutlet weak var countryPhoneCode:UILabel!
    @IBOutlet weak var countryTZ1:UILabel!
    @IBOutlet weak var countryTZ2:UILabel!
    @IBOutlet weak var countryTZ3:UILabel!
    @IBOutlet weak var countryCurrencyName:UILabel!
    @IBOutlet weak var bordersCollection:UICollectionView!
    @IBOutlet weak var saveBtn:UIButton!
    @IBOutlet weak var spinnerView:UIView!
    @IBOutlet weak var spinner:UIActivityIndicatorView!
    
    var countryCode = "0"
    
    var borders = [Borders]()
    
    var currentCountry = [String:Any]()
    var savedMode = false
    
    enum imagesURLs {
        case countryFlag
        func getURL() -> String{
            switch self {
            case .countryFlag:
                return "https://flagpedia.net/data/flags/w1160/"
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.spinnerView.isHidden = true
        self.spinner.isHidden = true
        self.spinner.stopAnimating()
        
        self.getDetail(self.countryCode)
        let countryFlagUrl = "\(imagesURLs.getURL(.countryFlag)())\(countryCode.lowercased()).png"
        self.getCountryImage(from: URL(string: countryFlagUrl)!)
        
        self.bordersCollection.delegate = self
        self.bordersCollection.dataSource = self
        
    }
    
    func getDetail(_ cc: String){
        
        self.spinnerView.isHidden = false
        self.spinner.isHidden = false
        self.spinner.startAnimating()
        
        CountriesAPI().getCountryDetail(countryCode: cc) { (jsonResponse, statusCode) in
            switch statusCode {
            case 200:
                self.iterateCountryDetail(contenido: jsonResponse)
                break;
            default:
                self.spinnerView.isHidden = true
                self.spinner.isHidden = true
                self.spinner.stopAnimating()
                SCLAlertView().showError("Error", subTitle: "\(jsonResponse)")
            }
        }
    }
    
    func iterateCountryDetail(contenido:JSON){
        
        let str = contenido.description
        let data = Data(str.utf8)

        do {
            // make sure this JSON is in the format we expect
            if let country = try JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String,Any> {
                
                self.currentCountry = country
                self.countryNameTitle.text = "\(country["name"] ?? "")"
                self.countryNameCapital.text = "\(country["capital"] ?? "")"
                self.countryNativeName.text = "\(country["nativeName"] ?? "")"
                self.countryMap.image = UIImage(named: "\(country["alpha3Code"] ?? "")")
                self.countrySubRegion.text = "\(country["subregion"] ?? "")"
                self.countryArea.text = "\(country["area"] ?? "") m2"
                let coords = country["latlng"] as! Array<Any>
                self.countryCoordinates.text = "( \(coords[0]),\(coords[1])  )"
                self.countryPopulation.text = "\(country["population"] ?? "")"
                let translations = country["translations"] as! Dictionary<String,Any>
                self.countryLanguage.text = "\(translations[Locale.current.languageCode ?? ""] ?? "")"
                self.countryPhoneCode.text = "\(country["numericCode"] ?? "")"
                let currencies = country["currencies"] as! Array<Any>
                self.countryCurrencyName.text = "\(currencies[0])"
                let timezones = country["timezones"] as! Array<Any>
                let a = timezones.indices.contains(0) ? "\(timezones[0])" : ""
                let b = timezones.indices.contains(1) ? "\(timezones[1])" : ""
                let c = timezones.indices.contains(2) ? "\(timezones[2])" : ""
                self.countryTZ1.text = a
                self.countryTZ2.text = b
                self.countryTZ3.text = c    
                let bordersArray = country["borders"] as! Array<Any>
                for border in bordersArray {
                    let borderObj = Borders(name: "\(border)")
                    self.borders.append(borderObj)
                }
                self.bordersCollection.reloadData()
            }
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
        
        self.spinnerView.isHidden = true
        self.spinner.isHidden = true
        self.spinner.stopAnimating()
        
    }
    
    
    func getCountryImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            // always update the UI from the main thread
            DispatchQueue.main.async() { [weak self] in
                self?.mainFlagImage.image = UIImage(data: data)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
//    MARK: CollectionView Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.borders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.bordersCollection.dequeueReusableCell(withReuseIdentifier: "bordersCell", for: indexPath) as! BordersCell
        
        cell.configureBordersCell(border: self.borders[indexPath.row])
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 116, height: 116)
    }
    
    @IBAction func dismissBtnPressed(_ sender:Any){
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func saveBtnPressed(){
        
        self.savedMode = self.savedMode == false ? true : false
        self.saveBtn.setTitle(self.savedMode == false ? "Save" : "Delete", for: .normal)
        
        if savedMode == true {
            UserDefaults.standard.setValue(self.currentCountry, forKey: "SavedCountry")
        }else{
            UserDefaults.standard.removeObject(forKey: "SavedCountry")
        }
        
    }
    
}
