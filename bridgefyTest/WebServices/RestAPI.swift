//
//  RestAPI.swift
//  bridgefyTest
//
//  Created by Ram on 21/04/21.
//

import UIKit
import SwiftyJSON
import Alamofire

class RestAPI {

    private typealias JSONStandard = [String: AnyObject]
    
    
    func generalRequest(_ uri: String, completion: @escaping (_ responseValue: JSON, _ statusCode: Int) -> ()) {
        
        let headers: HTTPHeaders = ["Accept": "application/json, text/plain, */*",
                                    "Accept-Encoding": "gzip, deflate, sdch, br",
                                    "Accept-Language": "es-ES,es;q=0.8",
                                    "Cache-Control":"no-cache",
                                    "Connection": "keep-alive",
                                    "Pragma" : "no-cache",
                                    "x-rapidapi-key":
                                    "c220045095msh0e500381f036195p1a352djsnb2b4a91b96e3",
                                    "x-rapidapi-host": "restcountries-v1.p.rapidapi.com"
                                    ]
        
        let alamoFireManager = Session.default

        alamoFireManager.request(uri, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (responseData) -> Void in
            
            let statusCode = responseData.response != nil ? responseData.response!.statusCode : 0
            
             if ((responseData.value) != nil) {
                let responseValue = JSON(responseData.value!)
                completion(responseValue, statusCode)
            } else {
                switch statusCode{
                case 204:
                    let errors = ["errors" : ["No data received"]]
                    let responseValue = JSON(errors)
                    let errorsString = String(describing: responseValue["errors"][0])
                    print("Received a: \(errorsString)")
                    completion(responseValue, statusCode)
                default:
                    let errors = ["errors" : ["Can't reach server, verify your internet connection"]]
                    let responseValue = JSON(errors)
                    let errorsString = String(describing: responseValue["errors"][0])
                    print("Received a: \(errorsString)")
                    completion(responseValue, statusCode)
                }
            }
        }
    }
    
}



