//
//  BordersCell.swift
//  bridgefyTest
//
//  Created by Ram on 22/04/21.
//

import UIKit

class BordersCell: UICollectionViewCell {
    
    @IBOutlet weak var flagLbl:UILabel!
    @IBOutlet weak var borderNameLbl:UILabel!
    
    func configureBordersCell(border:Borders){
        
        self.flagLbl.text = "\(GeneralHelpers().flag(border._name))"
        self.borderNameLbl.text = border._name
        
    }
    
    
}
